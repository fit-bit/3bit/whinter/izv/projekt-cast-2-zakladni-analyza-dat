#!/usr/bin/env python3.8
# coding=utf-8

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import os
# muzete pridat libovolnou zakladni knihovnu ci knihovnu predstavenou na prednaskach
# dalsi knihovny pak na dotaz

# Ukol 1: nacteni dat
def get_dataframe(filename: str, verbose: bool = False) -> pd.DataFrame:
    """
        - Funkce načte lokálně uložený soubor se statistikou nehod, jehož umístění je specifikováno
        argumentem filename.

        - Funkce vytvoří v načteném DataFrame sloupec ​ date , ​ který bude ve formátu pro reprezentaci
        data

        - S výjimkou sloupce region jsou sloupce vhodné reprezentovány pomocí kategorického
        datového typu.

        - Při povoleném výpisu ( ​ verbose == True​ ) je spočítána kompletní
        velikost všech sloupců v datovém rámci před a po úpravě na kategorické typy.
        orig_size=X MB
        new_size=X MB
        Čísla jsou vypisována na 1 desetinné místo a platí, že 1 MB = 1 048 576 B.
    """

    # Načtění souboru a vypsání velikosti
    df = pd.read_pickle(filename, compression = "gzip")
    if(verbose):
        print("orig_size = "+str(round(df.memory_usage(deep = True).sum() / 1048576, 1))+" MB")

    # Přidání sloupce date ze sloupce p2a a vytvoření vhodných kategorických typů
    df['date'] = pd.to_datetime(df['p2a'], format='%Y-%m-%d')
    
    for col in ['k', 'l', 'p', 'q', 't', 'h', 'i', 'j', 'p2a', 'p37', 'p2b', 'o', 'r', 's']:
        df[col] = df[col].astype('category')
    
    # Vypsání nové veliksoti
    if(verbose):
        print("new_size = "+str(round(df.memory_usage(deep = True).sum() / 1048576, 1))+"  MB")

    return df

# Ukol 2: následky nehod v jednotlivých regionech
def plot_conseq(df: pd.DataFrame, fig_location: str = None, show_figure: bool = False):
    """
    Funkce vytvoří graf následků nehod v jednotlivých regionech, které jsou uloženy do souboru
    specifikovaného argumentem ​ fig_location​ a případně zobrazeny pokud ​ show_figure ​je
    True​. Argument ​df​ odpovídá DataFrame jenž je výstupem funkce ​ get_dataframe​.
    Graf se skládá z 4 podgrafů organizovaných do matice tvořené jedním sloupcem a 4 řádky.
    """

    # Nachystání dat
    deaths = df[['region', 'p13a']].groupby('region').sum()
    heavy_injuries = df[['region', 'p13b']].groupby('region').sum()
    light_injuries = df[['region', 'p13c']].groupby('region').sum()
    all_accidents = df[['region', 'p1']].groupby('region').count()

    # Spravení indexů
    deaths = deaths.reset_index()
    heavy_injuries = heavy_injuries.reset_index()
    light_injuries = light_injuries.reset_index()
    all_accidents = all_accidents.reset_index()

    # Seřazení hodnot podle celkového počtu nehod
    all_accidents = all_accidents.sort_values('p1', ascending = False)

    deaths = deaths.reindex(all_accidents.index)
    heavy_injuries = heavy_injuries.reindex(all_accidents.index)
    light_injuries = light_injuries.reindex(all_accidents.index)

    # Vytvoření 4 podgrafů
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, sharex= True, figsize=(10,10))
    sns.set_style("whitegrid")

    ax1.set_title("Úmrtí")
    ax = sns.barplot(data=deaths, ax=ax1, x="region", y="p13a", color="red", ci=None)
    ax.set(ylabel='Počet', xlabel='Region')

    ax2.set_title("Těžce zranění")
    ax = sns.barplot(data=heavy_injuries, ax=ax2, x="region", y="p13b", color="darkorange", ci=None)
    ax.set(ylabel='Počet', xlabel='Region')

    ax3.set_title("Lehce zranění")
    ax = sns.barplot(data=light_injuries, ax=ax3, x="region", y="p13c", color="lime", ci=None)
    ax.set(ylabel='Počet', xlabel='Region')

    ax4.set_title("Celkem nehod")
    ax = sns.barplot(data=all_accidents, ax=ax4, x="region", y="p1", color="dodgerblue", ci=None)
    ax.set(ylabel='Počet', xlabel='Region')

    fig.tight_layout()

    # Uložení a zobrazení grafu podle parametrů
    if fig_location != None:
        if not os.path.exists(os.path.dirname(fig_location)):
            if(os.path.dirname(fig_location) != ""):
                os.makedirs(os.path.dirname(fig_location))

        fig.savefig(fig_location)

    if show_figure == True:
      plt.show()

# Ukol3: příčina nehody a škoda
def plot_damage(df: pd.DataFrame, fig_location: str = None, show_figure: bool = False):
    """
    Pro kraje (JHM - Jihomoravský, MSK - Moravskoslezský, PHA - Hlavní město Praha, OLK - Olomoucký)
    funkce znázorní počet nehod v závislosti na škodě na vozidlech
    uvedené v tisících Kč. Škoda je rozdělena do tříd podle hlavní příčiny nehody.
    """

    # Nachystání dat
    JHM_data = df[['region', 'p12', 'p53']].loc[df['region'] == "JHM"]
    MSK_data = df[['region', 'p12', 'p53']].loc[df['region'] == "MSK"]
    PHA_data = df[['region', 'p12', 'p53']].loc[df['region'] == "PHA"]
    OLK_data = df[['region', 'p12', 'p53']].loc[df['region'] == "OLK"]

    # Škoda zapsána v stokorunách, převod na tisicikoruny
    JHM_data.update(JHM_data['p53']/10)
    MSK_data.update(MSK_data['p53']/10)
    PHA_data.update(PHA_data['p53']/10)
    OLK_data.update(OLK_data['p53']/10)

    # Rozsekání do skupin podle typu nehody
    JHM_data.update(pd.cut(JHM_data['p12'], bins=[-np.inf, 200, 300, 400, 500, 600, np.inf], labels=["nezaviněná řidičem", "nepřiměřená rychlost jízdy", "nesprávné předjíždění", "nedání přednosti v jízdě", "nesprávný způsob jízdy", "technická závada vozidla"]))
    MSK_data.update(pd.cut(MSK_data['p12'], bins=[-np.inf, 200, 300, 400, 500, 600, np.inf], labels=["nezaviněná řidičem", "nepřiměřená rychlost jízdy", "nesprávné předjíždění", "nedání přednosti v jízdě", "nesprávný způsob jízdy", "technická závada vozidla"]))
    PHA_data.update(pd.cut(PHA_data['p12'], bins=[-np.inf, 200, 300, 400, 500, 600, np.inf], labels=["nezaviněná řidičem", "nepřiměřená rychlost jízdy", "nesprávné předjíždění", "nedání přednosti v jízdě", "nesprávný způsob jízdy", "technická závada vozidla"]))
    OLK_data.update(pd.cut(OLK_data['p12'], bins=[-np.inf, 200, 300, 400, 500, 600, np.inf], labels=["nezaviněná řidičem", "nepřiměřená rychlost jízdy", "nesprávné předjíždění", "nedání přednosti v jízdě", "nesprávný způsob jízdy", "technická závada vozidla"]))

    # Rozsekání do skupin podle částky
    JHM_data.update(pd.cut(JHM_data['p53'], bins=[-np.inf, 50, 200, 500, 1000, np.inf], labels=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"]))
    MSK_data.update(pd.cut(MSK_data['p53'], bins=[-np.inf, 50, 200, 500, 1000, np.inf], labels=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"]))
    PHA_data.update(pd.cut(PHA_data['p53'], bins=[-np.inf, 50, 200, 500, 1000, np.inf], labels=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"]))
    OLK_data.update(pd.cut(OLK_data['p53'], bins=[-np.inf, 50, 200, 500, 1000, np.inf], labels=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"]))

    # Sečtení počtu nehod pro jednotlivé skupiny
    JHM_data = JHM_data.groupby(['p12', 'p53']).count()
    MSK_data = MSK_data.groupby(['p12', 'p53']).count()
    PHA_data = PHA_data.groupby(['p12', 'p53']).count()
    OLK_data = OLK_data.groupby(['p12', 'p53']).count()

    # Spravení indexu
    JHM_data = JHM_data.reset_index()
    MSK_data = MSK_data.reset_index()
    PHA_data = PHA_data.reset_index()
    OLK_data = OLK_data.reset_index()

    # Přejmenování sloupců
    JHM_data = JHM_data.rename(columns={"region": "count", "p12": "Příčina nehody"})
    MSK_data = MSK_data.rename(columns={"region": "count", "p12": "Příčina nehody"})
    PHA_data = PHA_data.rename(columns={"region": "count", "p12": "Příčina nehody"})
    OLK_data = OLK_data.rename(columns={"region": "count", "p12": "Příčina nehody"})

    # Vytvoření 4 podgrafů
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(15,10))
    sns.set_style("whitegrid")

    ax1.set_title("JHM")
    ax = sns.barplot(data=JHM_data, ax=ax1, x="p53", y="count", hue="Příčina nehody", ci=None, order=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"])
    ax.set(ylabel='Počet', xlabel='Škoda [tisic Kč]')
    ax.set_yscale("log")
    ax.legend([],[], frameon=False)

    ax2.set_title("MSK")
    ax = sns.barplot(data=MSK_data, ax=ax2, x="p53", y="count", hue="Příčina nehody", ci=None, order=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"])
    ax.set(ylabel='Počet', xlabel='Škoda [tisic Kč]')
    ax.set_yscale("log")
    ax.legend([],[], frameon=False)

    ax3.set_title("PHA")
    ax = sns.barplot(data=PHA_data, ax=ax3, x="p53", y="count", hue="Příčina nehody", ci=None, order=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"])
    ax.set(ylabel='Počet', xlabel='Škoda [tisic Kč]')
    ax.set_yscale("log")
    ax.legend([],[], frameon=False)

    ax4.set_title("OLK")
    ax = sns.barplot(data=OLK_data, ax=ax4, x="p53", y="count", hue="Příčina nehody", ci=None, order=["< 50", "50 - 200", "200 - 500", "500 - 1000", ">1000"])
    ax.set(ylabel='Počet', xlabel='Škoda [tisic Kč]')
    ax.set_yscale("log")

    # Přesunutí legendy na lepší místo
    ax.legend(bbox_to_anchor=(1.05, 1), title="Příčina nehody" , loc=2, borderaxespad=0.)

    fig.tight_layout()

    # Uložení a zobrazení grafu podle parametrů
    if fig_location != None:
        if not os.path.exists(os.path.dirname(fig_location)):
            if(os.path.dirname(fig_location) != ""):
                os.makedirs(os.path.dirname(fig_location))

        fig.savefig(fig_location)

    if show_figure == True:
      plt.show()

# Ukol 4: povrch vozovky
def plot_surface(df: pd.DataFrame, fig_location: str = None, show_figure: bool = False):
    """
    Pro kraje (JHM - Jihomoravský, MSK - Moravskoslezský, PHA - Hlavní město Praha, OLK - Olomoucký) 
    funkce vykreslý čárový graf, který bude zobrazovat
    pro jednotlivé měsíce​ počet nehod při různém stavu povrchu vozovky.
    """
    
    # Nachystání dat
    test_data = df[['region', 'date', 'p16', 'p1']]
    test_data = pd.pivot_table(test_data, index=['region','date'], columns='p16', values='p1', aggfunc=np.count_nonzero)
    test_data = test_data.rename(columns={0: "jiný stav povrchu vozovky v době nehody", 1: "povrch suchý", 2: "povrch suchý", 3: "povrch mokrý", 4: "na vozovce je bláto", 5: "na vozovce je náledí, ujetý sníh", 6: "na vozovce je náledí, ujetý sníh", 7: "na vozovce je rozlitý olej, nafta apod.", 8: "souvislá sněhová vrstva, rozbředlý sníh", 9: "náhlá změna stavu vozovky"})

    #print(test_data.groupby('region').resample('M', on='date').sum())

    JHM_data = df[['region', 'date', 'p16', 'p1']].loc[df['region'] == "JHM"]
    MSK_data = df[['region', 'date', 'p16', 'p1']].loc[df['region'] == "MSK"]
    PHA_data = df[['region', 'date', 'p16', 'p1']].loc[df['region'] == "PHA"]
    OLK_data = df[['region', 'date', 'p16', 'p1']].loc[df['region'] == "OLK"]

    # Předělání tabulky podle stavu povrchu vozovky
    JHM_data = pd.pivot_table(JHM_data, index=['region','date'], columns='p16', values='p1', aggfunc=np.count_nonzero)
    MSK_data = pd.pivot_table(MSK_data, index=['region','date'], columns='p16', values='p1', aggfunc=np.count_nonzero)
    PHA_data = pd.pivot_table(PHA_data, index=['region','date'], columns='p16', values='p1', aggfunc=np.count_nonzero)
    OLK_data = pd.pivot_table(OLK_data, index=['region','date'], columns='p16', values='p1', aggfunc=np.count_nonzero)

    # Přejnenování sloupců na lepší jméno
    JHM_data = JHM_data.rename(columns={0: "jiný stav povrchu vozovky v době nehody", 1: "povrch suchý neznečištěný", 2: "povrch suchý znečištěný", 3: "povrch mokrý", 4: "na vozovce je bláto", 5: "na vozovce je náledí, ujetý sníh - posypané", 6: "na vozovce je náledí, ujetý sníh - neposypane", 7: "na vozovce je rozlitý olej, nafta apod.", 8: "souvislá sněhová vrstva, rozbředlý sníh", 9: "náhlá změna stavu vozovky"})
    MSK_data = MSK_data.rename(columns={0: "jiný stav povrchu vozovky v době nehody", 1: "povrch suchý neznečištěný", 2: "povrch suchý znečištěný", 3: "povrch mokrý", 4: "na vozovce je bláto", 5: "na vozovce je náledí, ujetý sníh - posypané", 6: "na vozovce je náledí, ujetý sníh - neposypane", 7: "na vozovce je rozlitý olej, nafta apod.", 8: "souvislá sněhová vrstva, rozbředlý sníh", 9: "náhlá změna stavu vozovky"})
    PHA_data = PHA_data.rename(columns={0: "jiný stav povrchu vozovky v době nehody", 1: "povrch suchý neznečištěný", 2: "povrch suchý znečištěný", 3: "povrch mokrý", 4: "na vozovce je bláto", 5: "na vozovce je náledí, ujetý sníh - posypané", 6: "na vozovce je náledí, ujetý sníh - neposypane", 7: "na vozovce je rozlitý olej, nafta apod.", 8: "souvislá sněhová vrstva, rozbředlý sníh", 9: "náhlá změna stavu vozovky"})
    OLK_data = OLK_data.rename(columns={0: "jiný stav povrchu vozovky v době nehody", 1: "povrch suchý neznečištěný", 2: "povrch suchý znečištěný", 3: "povrch mokrý", 4: "na vozovce je bláto", 5: "na vozovce je náledí, ujetý sníh - posypané", 6: "na vozovce je náledí, ujetý sníh - neposypane", 7: "na vozovce je rozlitý olej, nafta apod.", 8: "souvislá sněhová vrstva, rozbředlý sníh", 9: "náhlá změna stavu vozovky"})

    # Sečtení hodnot pr ojednotlivé měsíce a stavu vozovky
    JHM_data = JHM_data.reset_index()
    JHM_data = JHM_data.resample('M', on='date').sum()

    MSK_data = MSK_data.reset_index()
    MSK_data = MSK_data.resample('M', on='date').sum()

    PHA_data = PHA_data.reset_index()
    PHA_data = PHA_data.resample('M', on='date').sum()

    OLK_data = OLK_data.reset_index()
    OLK_data = OLK_data.resample('M', on='date').sum()

    # Poskládání dat pro zobrazení do grafu
    JHM_data = JHM_data.reset_index()
    JHM_data = JHM_data.melt(id_vars=['date'])

    MSK_data = MSK_data.reset_index()
    MSK_data = MSK_data.melt(id_vars=['date'])
    
    PHA_data = PHA_data.reset_index()
    PHA_data = PHA_data.melt(id_vars=['date'])
    
    OLK_data = OLK_data.reset_index()
    OLK_data = OLK_data.melt(id_vars=['date'])
    
    # Vytvoření 4 podgrafů
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(15,7))
    sns.set_style("whitegrid")

    ax1.set_title("JHM")
    ax = sns.lineplot(data=JHM_data, ax=ax1, x='date', y='value', hue='p16', ci=None)
    ax.set(ylabel='Počet nehod', xlabel='Datum vzniku nehody')
    ax.legend([],[], frameon=False)

    ax2.set_title("MSK")
    ax = sns.lineplot(data=MSK_data, ax=ax2, x='date', y='value', hue='p16', ci=None)
    ax.set(ylabel='Počet nehod', xlabel='Datum vzniku nehody')
    ax.legend([],[], frameon=False)

    ax3.set_title("PHA")
    ax = sns.lineplot(data=PHA_data, ax=ax3, x='date', y='value', hue='p16', ci=None)
    ax.set(ylabel='Počet nehod', xlabel='Datum vzniku nehody')
    ax.legend([],[], frameon=False)

    ax4.set_title("OLK")
    ax = sns.lineplot(data=OLK_data, ax=ax4, x='date', y='value', hue='p16', ci=None)
    ax.set(ylabel='Počet nehod', xlabel='Datum vzniku nehody')

    # Přesunutí legendy na lepší místo
    ax.legend(bbox_to_anchor=(1.05, 1), title="Stav vozovky" , loc=2, borderaxespad=0.)

    fig.tight_layout()

    # Uložení a zobrazení grafu podle parametrů
    if fig_location != None:
        if not os.path.exists(os.path.dirname(fig_location)):
            if(os.path.dirname(fig_location) != ""):
                os.makedirs(os.path.dirname(fig_location))

        fig.savefig(fig_location)

    if show_figure == True:
      plt.show()

if __name__ == "__main__":
    # zde je ukazka pouziti, tuto cast muzete modifikovat podle libosti
    # skript nebude pri testovani pousten primo, ale budou volany konkreni ¨
    # funkce.
    df = get_dataframe("accidents.pkl.gz", True)
    plot_conseq(df, "01_nasledky.png")
    plot_damage(df, "02_priciny.png")
    plot_surface(df, "03_stav.png")
